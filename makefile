PROJ_NAME=hello

CPP_SOURCE=$(wildcard ./source/*.cpp)

HPP_SOURCE=$(wildcard ./source/*.hpp)

OBJ=$(subst .c,.o,$(subst source,objects,$(CPP_SOURCE)))

CC=g++

CC_FLAGS=-c	\
	 -W 	\
	 -Wall  \
	 -ansi	\
	 -pedantic

RM = rm -rf

all: objFolder $(PROJ_NAME)

$(PROJ_NAME): $(OBJ)
	@ echo 'Building binary using G++ linker: $@'
	$(CC) $^ -O $@
	@ echo 'Finished building binary: $@'
	@ echo ' '
./objects/%.o: ./source/%.c ./headers/%.h
	@ echo 'Building target using G++ compiler: $<'
	$(CC) $< $(CC_FLAGS) -o $@
	@ echo ' ' 

./objects/main.o: ./source/main.c $(HPP_SOURCE)
	@ echo 'Building target using G++ compiler: $<'
	$(CC) $< $(CC_FLAGS) -o $@
	@ echo ' '

objFolder:
	@ mkdir -p objects

clean:
	@ $(RM) ./objects/*.o $(PROJ_NAME) *~
	@ rmdir objects

.PHONY: all clean

